import React from 'react';
import './App.css';
const defaultState = {
  timerLabel: 'Session',
  sessionLength: 25,
  breakLength: 5,
  countDownSession: 25,
  countDownBreak: 5,
  clockSeconds: 60,
  timeLeft: '25:00',
  clockCountsDown: false,
  clockCountingDown: 'session',
  clockIntervalHandlerID: null,
};
class Clock extends React.Component {
  constructor(props) {
    super(props);
    this.state = defaultState;
    this.reset = this.reset.bind(this);
    this.decrement = this.decrement.bind(this);
    this.increment = this.increment.bind(this);
    this.startPauseClockHandler = this.startPauseClockHandler.bind(this);
    this.startClock = this.startClock.bind(this);
    this.pauseClock = this.pauseClock.bind(this);
    this.intervalHandler = this.intervalHandler.bind(this);
    this.changeTimeLeft = this.changeTimeLeft.bind(this);
    this.sessionAndBreakHandler = this.sessionAndBreakHandler.bind(this);
  }
  componentDidMount() {
    this.audioTag = document.querySelector('#beep');
  }
  reset() {
    this.setState(defaultState);
    this.audioTag.pause();
    this.audioTag.load();
    clearInterval(this.clockIntervalHandlerID);
  }
  startPauseClockHandler() {
    const clockCountsDown = this.state.clockCountsDown;
    if (clockCountsDown) {
      this.pauseClock();
    } else {
      this.startClock();
    }
    this.setState({ clockCountsDown: !clockCountsDown });
  }
  startClock() {
    this.clockIntervalHandlerID = setInterval(this.intervalHandler, 100);
  }
  pauseClock() {
    clearInterval(this.clockIntervalHandlerID);
  }
  sessionAndBreakHandler(object) {
    let stateVar = object.stateVar;
    const action = object.action;

    if (action === 'increment') {
      this.increment(stateVar);
    } else {
      this.decrement(stateVar);
    }
  }
  decrement(stateVar) {
    const decremented = this.state[stateVar] - 1;

    if (!decremented <= 0) {
      if (stateVar === 'sessionLength') {
        if (!this.state.clockCountsDown) {
          this.changeTimeLeft({ minutes: decremented, seconds: 0 });
        }
        this.setState({
          sessionLength: decremented,
          countDownSession: decremented,
        });
      }
      if (stateVar === 'breakLength')
        this.setState({
          breakLength: decremented,
          countDownBreak: decremented,
        });
    }
  }
  increment(stateVar) {
    const incremented = this.state[stateVar] + 1;

    if (incremented < 61) {
      if (stateVar === 'sessionLength') {
        if (!this.state.clockCountsDown) {
          this.changeTimeLeft({ minutes: incremented, seconds: 0 });
        }
        this.setState({
          sessionLength: incremented,
          countDownSession: incremented,
        });
      }
      if (stateVar === 'breakLength')
        this.setState({
          breakLength: incremented,
          countDownBreak: incremented,
        });
    }
  }
  intervalHandler() {
    if (this.state.clockCountingDown === 'session') {
      this.countDownSession();
      this.setState({ timerLabel: 'Session' });
      return;
    }

    if (this.state.clockCountingDown === 'break') {
      this.countDownBreak();
      this.setState({ timerLabel: 'Break' });
      return;
    }

    let sessionLength = this.state.sessionLength;
    let breakLength = this.state.breakLength;
    this.setState({
      countDownSession: sessionLength,
      countDownBreak: breakLength,
      clockCountingDown: 'session',
    });
    this.intervalHandler();
  }
  countDownSession() {
    if (this.state.clockSeconds === 60) {
      this.changeTimeLeft({
        minutes: this.state.countDownSession,
        seconds: 0,
      });
      let seconds = this.state.clockSeconds - 1;
      this.setState({
        clockSeconds: seconds,
      });
      return;
    }
    let clockSeconds = this.state.clockSeconds - 1;
    let countDownSession = this.state.countDownSession - 1;
    this.changeTimeLeft({
      minutes: countDownSession,
      seconds: clockSeconds,
    });
    if (clockSeconds === 0) {
      if (countDownSession === 0) {
        this.audioTag.play();
        this.setState({ clockCountingDown: 'break' });
      }
      this.setState({ countDownSession, clockSeconds: 60 });
      this.changeTimeLeft({
        minutes: countDownSession,
        seconds: clockSeconds,
      });
      return;
    }
    this.setState({ clockSeconds });
  }
  countDownBreak() {
    if (this.state.clockSeconds === 60) {
      this.changeTimeLeft({
        minutes: this.state.countDownBreak,
        seconds: 0,
      });
      let seconds = this.state.clockSeconds - 1;
      this.setState({
        clockSeconds: seconds,
      });
      return;
    }

    let clockSeconds = this.state.clockSeconds - 1;
    let countDownBreak = this.state.countDownBreak - 1;
    this.changeTimeLeft({
      minutes: countDownBreak,
      seconds: clockSeconds,
    });
    this.changeTimeLeft({
      minutes: countDownBreak,
      seconds: clockSeconds,
    });
    if (clockSeconds === 0) {
      if (countDownBreak === 0) {
        this.audioTag.play();
        this.setState({ clockCountingDown: '' });
      }
      this.setState({ countDownBreak, clockSeconds: 60 });
      this.changeTimeLeft({
        minutes: countDownBreak,
        seconds: clockSeconds,
      });
      return;
    }
    this.setState({ clockSeconds });
  }
  changeTimeLeft(time) {
    let minutes = time.minutes;
    let seconds = time.seconds;

    if (minutes < 10) {
      minutes = minutes.toString();
      minutes = `0${minutes}`;
    }
    if (seconds < 10) {
      seconds = seconds.toString();
      seconds = `0${seconds}`;
    }
    let timeString = `${minutes}:${seconds}`;

    this.setState({
      timeLeft: timeString,
    });
  }
  render() {
    return (
      <div className='clock'>
        <div id='timer'>
          <div id='timer-label'>{this.state.timerLabel}</div>
          <div id='time-left'>{this.state.timeLeft}</div>
        </div>
        <div id='session'>
          <span id='session-label'>Session Length: </span>
          <span id='session-length'>{this.state.sessionLength}</span>
          <div id='controls'>
            <input
              onClick={() => {
                this.sessionAndBreakHandler({
                  stateVar: 'sessionLength',
                  action: 'decrement',
                });
              }}
              id='session-decrement'
              type='button'
              value='&darr;'
            />
            <input
              onClick={() => {
                this.sessionAndBreakHandler({
                  stateVar: 'sessionLength',
                  action: 'increment',
                });
              }}
              id='session-increment'
              type='button'
              value='&uarr;'
            />
          </div>
        </div>
        <div id='break'>
          <span id='break-label'>Break Length: </span>
          <span id='break-length'>{this.state.breakLength}</span>
          <div id='controls'>
            {' '}
            <input
              onClick={() => {
                this.sessionAndBreakHandler({
                  stateVar: 'breakLength',
                  action: 'decrement',
                });
              }}
              id='break-decrement'
              type='button'
              value='&darr;'
            />
            <input
              onClick={() => {
                this.sessionAndBreakHandler({
                  stateVar: 'breakLength',
                  action: 'increment',
                });
              }}
              id='break-increment'
              type='button'
              value='&uarr;'
            />
          </div>
        </div>

        <input
          onClick={this.startPauseClockHandler}
          type='button'
          id='start_stop'
          value='start_stop'
        />
        <input onClick={this.reset} type='button' id='reset' value='reset' />
        <audio
          id='beep'
          src='https://raw.githubusercontent.com/freeCodeCamp/cdn/master/build/testable-projects-fcc/audio/BeepSound.wav'
        ></audio>
      </div>
    );
  }
}

export default Clock;
